var builder = require('./lib/builder');

var config = {
	metaTitle: '',
	metaTitlePrefix: 'Comic EL GRITO del Tue Tue'
};

(async() => {


	await builder.transformFile({
		target: '/index.html',
		source: "src/templates/home.pug",
		mode: 'pug',
		context: {
			...config
		}
	});

	await builder.transformFile({
		target: '/el-autor/index.html',
		source: "src/templates/el-autor.pug",
		mode: 'pug',
		context: {
			...config
		}
	});

	await builder.transformFile({
		target: '/contact/index.html',
		source: "src/templates/contact.pug",
		mode: 'pug',
		context: {
			...config
		}
	});



	await builder.transformFile({
		target: '/css/styles.css',
		source: "src/styles.css",
		mode: 'scss'
	})


	await builder.copy('src/public', 'dist')

	console.log('builder done')

})();